import 'package:alterador_de_tema/home_page.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String email =
      ""; // criando variavel para pegar informação do e-mail do usuario
  String senha =
      ""; // criando variavel para pegar informação do senha do usuario

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Image.network(
              'https://png.pngtree.com/thumb_back/fh260/background/20190221/ourmid/pngtree-technology-background-technological-sense-science-fiction-modern-image_15121.jpg',
              fit: BoxFit.cover,
            ),
          ),
          Container(
            color: Colors.black.withOpacity(0.2), // mascara para deixar escuro
          ),
          SingleChildScrollView(
            // deixa um scrool na pagina
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 200, // Largura da imagem
                      height: 200, // altura da imagem
                      child: Image.network(
                          'https://duckworthbooks.com/wp-content/uploads/2017/06/logo-tv-logo-300x295.png'),
                    ), // colocando uma imagem da internet

                    TextField(
                      onChanged: (Text) {
                        email = Text;
                      },
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: 'Email',
                        border: OutlineInputBorder(),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextField(
                      onChanged: (Text) {
                        senha = Text;
                      },
                      obscureText: true, // faz a senha não ficar visivel
                      decoration: InputDecoration(
                        labelText: 'Senha',
                        border:
                            OutlineInputBorder(), // deixa a borda meia arredondada
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                      onPressed: () {
                        if (email == 'teste@gmail.com' && senha == '123') {
                          Navigator.of(context).pushReplacementNamed(
                              '/home'); // navegação com rotas sem o botão de voltar

                          // Navigator.of(context).pushReplacement(MaterialPageRoute(
                          //   builder: (context) =>
                          //     HomePage())); // Navegação para a HomePage sem o botão de voltar por conta do (pushReplacement)

                          //Navigator.of(context).push(
                          // MaterialPageRoute(builder: (context) => HomePage())); navegação com o botão de voltar
                        } else {
                          print('Incorreto');
                        }
                      },
                      child: Text('Entrar'),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
