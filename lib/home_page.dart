import 'package:alterador_de_tema/app_controller.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        // Menu da tela de inicio(Drawer)
        child: Column(
          children: [
            UserAccountsDrawerHeader(
              // conta do usuario com accountName - nome e accountEmail email
              currentAccountPicture: ClipRRect(
                // ClipRRect corta a imagem da forma que decidirmos
                // borderRadius: BorderRadius.circular(1000), // para deixar redondo
                child: Image.network(
                    'https://cdn.icon-icons.com/icons2/65/PNG/128/user_add_12818.png'),
              ), // adicionando uma imagem para o usuario
              accountName: Text('Teste'),
              accountEmail: Text('teste@gmail.com'),
            ),
            ListTile(
              // Lista de itens já pré construido (ListTile)
              leading: Icon(Icons.home), // Colocar um item na extremidade
              title: Text('Inicio'),
              subtitle: Text('Tela de inicio'),
              onTap: () {
                // ação de um clicke
                debugPrint('clicou no inicio');
              },
            ),
            ListTile(
              // Lista de itens já pré construido (ListTile)
              leading:
                  Icon(Icons.highlight_off), // Colocar um item na extremidade
              title: Text('Sair'),
              subtitle: Text('Finalizar sesão'),
              onTap: () {
                // ação de um clicke
                Navigator.of(context).pushReplacementNamed(
                    '/'); // rota para voltar a tela inicial do app
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text('Mude a cor do app'),
        actions: [CustomSwitch()],
      ),
      body: Center(
        child: CustomSwitch(),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            setState(() {
              counter++;
            });
          }),
    );
  }
}

class CustomSwitch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Switch(
        value: AppController.instance.isDartTheme,
        onChanged: (value) {
          AppController.instance.changeTheme();
        });
  }
}
