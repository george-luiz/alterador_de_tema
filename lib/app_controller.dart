import 'package:flutter/material.dart';

class AppController extends ChangeNotifier {
  static AppController instance = AppController(); // instanciando ele mesmo

  bool isDartTheme = false;
  changeTheme() {
    isDartTheme =
        !isDartTheme; // (! com o ponto de exclamação na variavel) se é false vira true se é true vira false
    notifyListeners(); // irá notificar
  }
}
